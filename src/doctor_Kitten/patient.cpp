#include "patient.h"

Illness::Illness(string title, string sympt[], string fullname, bool easily) {
    this->_title= title;
    this->_doctorFullname = fullname;
    for (int i = 0; i < 4; i++) this->_symptoms[i] = sympt[i];
    this->_easilyCurable = easily;
}

Illness::~Illness(){}
string Illness::titleIllness() { return this->_title;}

string* Illness::symptomsIllness() { return this->_symptoms;}

string Illness::doctorFullnameIllness() { return this->_doctorFullname;}

bool Illness::easilyCurable() {return this->_easilyCurable;}


Patient::Patient(int id, string surname, string name, int date[])
{
    this->_id = id;
    this->_surmane = surname;
    this->_name = name;
    for (int i = 0; i < 3; i++) this->_date[i] = date[i];
    if (date[2] >= 18) this->_isAdult = true;
    else this->_isAdult = false;
    this->_isHealthy = false;
}
Patient::~Patient(){}

int Patient::id() {return this->_id;}

string Patient::surname() {return this->_surmane;}

string Patient::name() {return this->_name;}

int* Patient::date() {return this->_date;}

bool Patient::isAdult() {return this->_isAdult;}

bool Patient::isFree() {return this->_isHealthy;}

void Patient::set_isHealthy(bool is) {
    this->_isHealthy = is;
}

void Patient::set_Picture(string filename) {
    this->PicFilename = filename;
}

void Patient::set_Illness(string title, string sympt[], string fullname, bool easily) {
    this->illness = new Illness(title,sympt,fullname,easily);
}

string Patient::titleIllness() {
    return this->illness->titleIllness();
}

string* Patient::symptomsIllness() {
    return this->illness->symptomsIllness();
}

string Patient::doctorFullnameIllness() {
    return this->illness->doctorFullnameIllness();
}

bool Patient::easilyCurableIllness() {
    return this->illness->easilyCurable();
}
