#ifndef DATABASE_H
#define DATABASE_H
#include <iostream>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>

using namespace std;

class database
{
    QSqlDatabase db;
public:
    database();
    bool insertPateint(int id, QString surname, QString name, QString date, QString status, QString checkin, QString sex);
    void selectPatients();
    int sizePatients();
    bool deletePatient(QString id);
    bool existsPatient(QString id);
    bool deleteDesease(QString id);
    bool deleteImage(QString id);
    bool insertDesease(int id, QString type, QString doctor, QString appointment, QString status);
    bool existsDoctor(QString name);
    QStringList getAllDoctors();
};

#endif // DATABASE_H
