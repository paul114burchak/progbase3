#ifndef PATIENT_H
#define PATIENT_H

#include <iostream>
using namespace std;

class Illness
{
    string _title;
    string _symptoms[4];
    string _doctorFullname;
    bool _easilyCurable;
public:
    Illness(string title, string sympt[], string fullname, bool easily);
    ~Illness();
    string titleIllness();
    string* symptomsIllness();
    string doctorFullnameIllness();
    bool easilyCurable();
};

class Patient
{
    int _id;
    string _surmane;
    string _name;
    int _date[3];
    bool _isAdult;
    bool _isHealthy;
    string PicFilename;
    Illness* illness;
public:
    Patient(int id, string surname, string name, int date[]);
    ~Patient();
    int id();
    string surname();
    string name();
    int* date();
    bool isAdult();
    bool isFree();
    void set_isHealthy(bool is);
    void set_Picture(string filename);
    void set_Illness(string title, string sympt[], string fullname, bool easily);
    string titleIllness();
    string* symptomsIllness();
    string doctorFullnameIllness();
    bool easilyCurableIllness();
};

#endif // PATIENT_H
