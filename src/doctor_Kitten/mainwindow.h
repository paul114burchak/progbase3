#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "patient.h"
#include <QTableWidget>
#include <QFileDialog>
#include <database.h>
#include <QListWidgetItem>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    database *db;

    void setTableRow(QString id, QString surname, QString name, QDate date, int row);
    void setTable (void);
    void setCalendar(void);
    void setCalendarRow(QString _type, QString _appointment, QString _id, QString _surname, int row);
    void selectByCriterio(QString criterio, QString arg1);
private slots:
    void on_pushButton_clicked();

    void on_tableWidget_cellDoubleClicked(int row, int column);
    
    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_checkBox_stateChanged(int arg1);

    void on_lineEdit_textEdited(const QString &arg1);

    void on_listWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_comboBox_2_currentIndexChanged(const QString &arg1);

    void on_pushButton_6_clicked();

    void on_pushButton_2_clicked();

    void on_tabWidget_currentChanged(int index);

    void on_tableWidget_2_cellDoubleClicked(int row, int column);

private:

    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
