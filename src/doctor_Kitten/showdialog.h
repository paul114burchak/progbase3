#ifndef SHOWDIALOG_H
#define SHOWDIALOG_H
#include <QListWidgetItem>
#include <QDialog>

namespace Ui {
class ShowDialog;
}

class ShowDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ShowDialog(QWidget *parent = 0);
    void setData(QString id);
    void insertItemToList(QString _type, QString _doctor, QString _appointment, QString _status, int row);
    void selectDesease(QString id);

    void clear();
    ~ShowDialog();

private slots:
    void on_pushButton_5_clicked();
    
    void on_pushButton_6_clicked();
    
    void on_pushButton_3_clicked();
    
    void on_pushButton_2_clicked();
    
    void on_lineEdit_4_textEdited(const QString &arg1);

    void on_dateTimeEdit_2_dateTimeChanged(const QDateTime &dateTime);

    void on_lineEdit_3_textEdited(const QString &arg1);

    void on_pushButton_clicked();

    void on_pushButton_4_clicked();

    void on_listWidget_itemSelectionChanged();

    void on_pushButton_7_clicked();

    void on_comboBox_currentTextChanged(const QString &arg1);

    void on_pushButton_8_clicked();

private:
    Ui::ShowDialog *ui;
};

#endif // SHOWDIALOG_H
