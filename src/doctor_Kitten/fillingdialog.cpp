#include "fillingdialog.h"
#include "ui_fillingdialog.h"
#include <QComboBox>

FillingDialog::FillingDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FillingDialog)
{
    ui->setupUi(this);
    ui->dateTimeEdit->setMinimumDateTime(QDateTime::currentDateTime());
    ui->lineEdit->setEnabled(false);
    QString spec = parent->findChild<QComboBox*>("comboBox_3")->currentText();
    if(QString::compare(spec, "Surgeon",Qt::CaseInsensitive)== 0) {
        ui->comboBox->removeItem(1);
        ui->comboBox->removeItem(2);
        ui->comboBox->removeItem(3);
    }
    if(QString::compare(spec, "Dentist",Qt::CaseInsensitive)== 0) {
        ui->comboBox->removeItem(0);
        ui->comboBox->removeItem(1);
        ui->comboBox->removeItem(1);
        ui->comboBox->removeItem(1);
        ui->comboBox->removeItem(1);
    }
    if(QString::compare(spec, "Traumatologist",Qt::CaseInsensitive)== 0) {
        ui->comboBox->removeItem(3);
        ui->comboBox->removeItem(4);
    }
    if(QString::compare(spec, "Dermatologist",Qt::CaseInsensitive)== 0) {
        ui->comboBox->removeItem(0);
        ui->comboBox->removeItem(0);
        ui->comboBox->removeItem(0);
        ui->comboBox->removeItem(0);
        ui->comboBox->removeItem(1);
    }
    if(QString::compare(spec, "Cardiologist",Qt::CaseInsensitive)== 0) {
        ui->comboBox->removeItem(0);
        ui->comboBox->removeItem(0);
        ui->comboBox->removeItem(1);
        ui->comboBox->removeItem(1);
        ui->comboBox->removeItem(1);
    }
    if(QString::compare(spec, "Therapist",Qt::CaseInsensitive)== 0) {
        ui->comboBox->removeItem(0);
        ui->comboBox->removeItem(0);
        ui->comboBox->removeItem(0);
        ui->comboBox->removeItem(1);
    }
}

FillingDialog::~FillingDialog()
{
    delete ui;
}

void FillingDialog::on_pushButton_clicked()
{

    ui->lineEdit->clear();
    ui->lineEdit_1->clear();
    ui->lineEdit_2->clear();
    ui->checkBox->setCheckState(Qt::CheckState(0));
    ui->checkBox_2->setCheckState(Qt::CheckState(0));
    ui->dateTimeEdit->setDateTime(QDateTime::currentDateTime());
}

void FillingDialog::on_checkBox_stateChanged(int arg1)
{
    if (arg1 == 2) {
        ui->comboBox->setEnabled(false);
        ui->comboBox_2->setEnabled(false);
        ui->dateTimeEdit->setEnabled(false);
    }
    else {
        ui->comboBox->setEnabled(true);
        ui->dateTimeEdit->setEnabled(false);
        ui->comboBox_2->setEnabled(true);
    }
}

void FillingDialog::on_comboBox_currentIndexChanged(const QString &arg1)
{
    QStringList head = (QStringList() << "Headache" << "Dizziness" << "External injury");
    QStringList teeth = (QStringList() << "Toothache" << "Caries" << "External injury");
    QStringList heart = (QStringList() << "Heartache" << "Insult" << "External injury" << "High preasure");
    QStringList breathing = (QStringList() << "Cough" << "Sneezing" << "Hard to breath");
    QStringList skin = (QStringList() << "Burn" << "Redness" << "Dryness" << "Pimples" << "Injury");
    QStringList temp = (QStringList() << "Cold" << "Infection");
    auto combo2 = this->ui->comboBox_2;

    combo2->setEnabled(true);
    ui->lineEdit->setEnabled(false);

    if (QString::compare(arg1, "Head",Qt::CaseInsensitive)== 0) {
        combo2->clear();
        combo2->addItems(head);
    }
    if (QString::compare(arg1, "Teeth",Qt::CaseInsensitive)== 0) {
        combo2->clear();
        combo2->addItems(teeth);
    }
    if (QString::compare(arg1, "Heart",Qt::CaseInsensitive)== 0) {
        combo2->clear();
        combo2->addItems(heart);
    }
    if (QString::compare(arg1, "Breathing",Qt::CaseInsensitive)== 0) {
        combo2->clear();
        combo2->addItems(breathing);
    }
    if (QString::compare(arg1, "Skin",Qt::CaseInsensitive)== 0) {
        combo2->clear();
        combo2->addItems(skin);
    }
    if (QString::compare(arg1, "Body temperature",Qt::CaseInsensitive)== 0) {
        combo2->clear();
        combo2->addItems(temp);
    }
    if (QString::compare(arg1, "Another",Qt::CaseInsensitive)== 0) {
        combo2->clear();
        combo2->setEnabled(false);
        ui->lineEdit->setEnabled(true);
    }
}

void FillingDialog::on_checkBox_2_stateChanged(int arg1)
{
    if (arg1 == 2) {
        ui->comboBox->setEnabled(false);
        ui->comboBox_2->setEnabled(false);
        ui->dateTimeEdit->setEnabled(false);
        ui->checkBox->setCheckState(Qt::CheckState(0));
    }
    else {
        ui->comboBox->setEnabled(true);
        ui->comboBox_2->setEnabled(true);
        ui->dateTimeEdit->setEnabled(true);
    }
}
