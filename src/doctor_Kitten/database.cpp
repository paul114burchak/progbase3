#include "database.h"
#include <QDebug>
#include <iostream>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTableWidget>
#include <QByteArray>
#include <QFile>

using namespace std;

database::database() {
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("/home/firefly/op/progbase3/src/database");
    if(!db.open()) {
        qDebug() << "ERROR: " << db.lastError();
    } else {
        qDebug() << "Database: connection ok";
    }
}


bool database::insertPateint(int id, QString surname, QString name, QString date, QString status, QString checkin, QString sex) {
    QSqlQuery query;
    query.prepare("INSERT INTO patients (ID, Surname, Name, Date_birth, Status, Registration_date, sex) VALUES (:ID, :Surname, :Name, :Date_birth, :Status, :Registration_date, :sex)");
    query.bindValue(":ID",id);
    query.bindValue(":Surname",surname);
    query.bindValue(":Name",name);
    query.bindValue(":Date_birth",date);
    query.bindValue(":Status",status);
    query.bindValue(":Registration_date",checkin);
    query.bindValue(":sex",sex);

    QString filename = "/home/firefly/op/progbase3/src/nopicture.png";
    QFile file(filename);

    if (!file.open(QIODevice::ReadOnly)) return false;
    QByteArray inByteArray = file.readAll();
    QSqlQuery qry;
    qry.prepare( "INSERT INTO images (ID, filename, picture) VALUES (:ID, :filename, :picture)" );
    qry.bindValue( ":ID", id );
    qry.bindValue( ":picture", inByteArray );
    qry.bindValue( ":filename", filename );
    if( !qry.exec() )
        qDebug() << "Error inserting image into table:\n" << qry.lastError();

    if (query.exec()) return true;
    qDebug() << "insertPateint error:  "
                     << query.lastError();
    return false;
}

bool database::insertDesease(int id, QString type, QString doctor, QString appointment, QString status) {
    QSqlQuery query;
    query.prepare("INSERT INTO desease (ID, doctor, appointment, status, type) VALUES (:ID, :doctor, :appointment, :status, :type)");
    query.bindValue(":ID",id);
    query.bindValue(":doctor",doctor);
    query.bindValue(":appointment",appointment);
    query.bindValue(":status",status);
    query.bindValue(":type",type);
    if( !query.exec() ) {
        qDebug() << "Error inserting desease\n" << query.lastError();
        return false;
    }
    return true;
}

void database::selectPatients() {
    QSqlQuery query("SELECT * FROM patients");
    int idId = query.record().indexOf("ID");

    int idName = query.record().indexOf("Name");

    int idSurname = query.record().indexOf("Surname");

    int idDate = query.record().indexOf("Date ");
    int row = 0;
    while (query.next())
    {
       QString id = query.value(idId).toString();

       QString name = query.value(idName).toString();

       QString surname = query.value(idSurname).toString();

       QString date = query.value(idDate).toString();
       qDebug() << id << name << surname << date;
       //setTableRow(id,surname,name,date,row);
       row++;
    }
}

int database::sizePatients(void) {
    QSqlQuery qry;
    qry.prepare("SELECT * FROM patients");
    qry.exec();
    qry.last();
    return qry.at() + 1;
}

bool database::deletePatient(QString id) {
    QSqlQuery query;
    query.prepare("DELETE FROM patients WHERE ID = (:ID)");
    query.bindValue(":ID", id);
    if(!query.exec())
    {
        qDebug() << "removePerson error: " << query.lastError();
        return false;
    }
    return true;
}

bool database::deleteImage(QString id) {
    QSqlQuery query;
    query.prepare("DELETE FROM images WHERE ID = (:ID)");
    query.bindValue(":ID", id);
    if(!query.exec())
    {
        qDebug() << "removeImages error: " << query.lastError();
        return false;
    }
    return true;
}

bool database::deleteDesease(QString id) {
    QSqlQuery query;
    query.prepare("DELETE FROM desease WHERE ID = (:ID)");
    query.bindValue(":ID", id);
    if(!query.exec())
    {
        qDebug() << "removeDeseases error: " << query.lastError();
        return false;
    }
    return true;
}

bool database::existsPatient(QString id) {
    QSqlQuery query;
    query.prepare("SELECT ID FROM patients WHERE ID = (:ID)");
    query.bindValue(":ID", id);

    if (query.exec())
    {
       if (query.next())
       {
          qDebug() << "exists" << id <<endl;
          return true;
       }
    }
    return false;
}

bool database::existsDoctor(QString name) {
    QSqlQuery query;
    query.prepare("SELECT name FROM doctors WHERE name = (:name)");
    query.bindValue(":name", name);

    if (query.exec())
    {
       if (query.next())
       {
          qDebug() << "exists" <<endl;
          return true;
       }
    }
    return false;
}

QStringList database::getAllDoctors(){
    QStringList doctors;
    QSqlQuery query("SELECT * FROM doctors");
    int _doctor = query.record().indexOf("name");
    while (query.next()){
        QString doctor = query.value(_doctor).toString();
        doctors.append(doctor);
    }
    return doctors;
}
