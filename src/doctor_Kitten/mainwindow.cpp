#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDialog>
#include "fillingdialog.h"
#include "ui_fillingdialog.h"
#include "showdialog.h"
#include <QDateEdit>
#include <QDate>
#include <QTableWidget>
#include <QFileDialog>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QListWidgetItem>
#include <QSqlError>
#include <QDateTimeEdit>
#include <QSqlQuery>
#include <QDebug>
#include <QComboBox>
#include <QCheckBox>
#include <QStringList>
#include <QVariant>
#include <aboutdialog.h>
#include <QDateTime>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    auto table = ui->tableWidget;

    auto table2 = ui->tableWidget_2;

    ui->pushButton->setEnabled(false);
    ui->tabWidget->setCurrentIndex(0);

    ui->comboBox_2->setCurrentIndex(-1);
    ui->comboBox_3->setCurrentIndex(0);
    table->setColumnCount(4);
    table->verticalHeader()->hide();
    table->horizontalHeader()->show();
    table->setShowGrid(true);
    table->setEditTriggers(QAbstractItemView::NoEditTriggers);
    table->setSelectionBehavior(QAbstractItemView::SelectRows);
    table->setSelectionMode(QAbstractItemView::SingleSelection);

    table2->setColumnCount(4);
    table2->verticalHeader()->hide();
    table2->horizontalHeader()->show();
    table2->setShowGrid(true);
    table2->setEditTriggers(QAbstractItemView::NoEditTriggers);
    table2->setSelectionBehavior(QAbstractItemView::SelectRows);
    table2->setSelectionMode(QAbstractItemView::SingleSelection);
    db = new database();
    table->setRowCount(db->sizePatients());
    this->setWindowTitle("Doctor Kitten");
    setTable();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{

    auto table = ui->tableWidget;
    int row = table->rowCount();
    FillingDialog *dialog = new FillingDialog(this);
    dialog->show();
    if (dialog->exec()) {
        QLineEdit * surnameEdit = dialog->findChild<QLineEdit*>("lineEdit_2");
        QLineEdit * nameEdit = dialog->findChild<QLineEdit*>("lineEdit_1");
        QLineEdit * tEdit = dialog->findChild<QLineEdit*>("lineEdit");
        QComboBox * combo = dialog->findChild<QComboBox*>("comboBox");
        QComboBox * combo2 = dialog->findChild<QComboBox*>("comboBox_2");
        QComboBox * combo3 = dialog->findChild<QComboBox*>("comboBox_3");
        QCheckBox * statusOk = dialog->findChild<QCheckBox*>("checkBox");
        QCheckBox * firstAid = dialog->findChild<QCheckBox*>("checkBox_2");
        QDateTimeEdit * dateTime = dialog->findChild<QDateTimeEdit*>("dateTimeEdit");
        QDate date = dialog->ui->dateEdit->date();
        QString status;
        QDateTime checkin = QDateTime::currentDateTime();

        QDateTime appointment = dateTime->dateTime();
        QString problem = combo->currentText();
        QString symptoms = combo2->currentText();
        QString sex = combo3->currentText();
        QString dr =ui->lineEdit_2->text();

        if (!dr.contains("Dr.")) dr = "Dr. " +dr;
        if (!combo->isEnabled()) status = "Healthy";
        else status = "Deseased";
        if (firstAid->checkState() == 2) {
            status = "Heavy injury";
            appointment = checkin;
            symptoms = "First Aid";
        }

        if(combo->currentIndex() == 6) {
            symptoms = ui->lineEdit->text();
        }
        if(QString::compare(combo2->currentText(), "Another",Qt::CaseInsensitive)== 0) {
            symptoms = tEdit->text();
        }

        if (statusOk->checkState() == 2) {
            status = "Ok";
            symptoms = "No";
        }

        if(QString::compare(symptoms, "External injury",Qt::CaseInsensitive)== 0) {
            symptoms = problem + symptoms;
        }

        int id = 7 * db->sizePatients() + 1000 + row;
        db->insertPateint(id, surnameEdit->text(),
                          nameEdit->text(),
                          date.toString("dd MM yyyy"),
                          status,
                          checkin.toString("dd.MM.yyyy hh:mm"),sex);
        row++;
        table->setRowCount(row);
        setTableRow(QString::number(id),surnameEdit->text(),nameEdit->text(),date,row-1);
        db->insertDesease(id,symptoms,dr,appointment.toString("dd.MM.yyyy hh:mm"),"Started treatment");
    }
    dialog->hide();
    delete dialog;
}

void MainWindow::on_tableWidget_cellDoubleClicked(int row, int column)
{
    QString id = ui->tableWidget->item(row,0)->text();
    ShowDialog dialog;
    dialog.setData(id);
    dialog.exec();
}

void MainWindow::setTableRow(QString id, QString surname, QString name, QDate date, int row) {
    auto table = ui->tableWidget;
    table->setItem(row,0,new QTableWidgetItem(id));
    table->setItem(row,1,new QTableWidgetItem(surname));
    table->setItem(row,2,new QTableWidgetItem(name));
    table->setItem(row,3,new QTableWidgetItem(QString::number(2017 - date.year())));
}

void MainWindow::on_pushButton_4_clicked()
{
    ui->tableWidget->setRowCount(0);
    this->ui->tableWidget->setRowCount(db->sizePatients());
    setTable();
}

void MainWindow::setTable (void) {
    QSqlQuery query("SELECT * FROM patients");

    int idId = query.record().indexOf("ID");
    int idName = query.record().indexOf("Name");
    int idSurname = query.record().indexOf("Surname");
    int idDate = query.record().indexOf("Date_birth");
    int row = 0;
    if (!query.next()) qDebug() << "empty";
    do {
       QString id = query.value(idId).toString();
       QString name = query.value(idName).toString();
       QString surname = query.value(idSurname).toString();
       QDate date = QDate::fromString(query.value(idDate).toString(),"dd MM yyyy");
       setTableRow(id,surname,name,date,row);
       row++;
    } while (query.next());
}

void MainWindow::on_pushButton_5_clicked()
{
    QString name =  "Dr. " + ui->lineEdit_2->text();
    QString spec = ui->comboBox_3->currentText();

    QSqlQuery query;
    query.prepare("INSERT INTO doctors (name, spec) VALUES ('" +name +"' , '" +spec +"')");
    if (!db->existsDoctor(name)) {
        if (!query.exec()) qDebug() << "error" << query.lastError();
        ui->pushButton->setEnabled(true);
    } else {
        ui->label_3->setText("Error: You have just signed in, choose your name from Box");
        ui->checkBox->setCheckState(Qt::CheckState(2));
    }

    ui->label_6->setText(name);
    ui->label_7->setText(spec);
    ui->pushButton_5->setEnabled(false);
    ui->comboBox_2->setCurrentText(name);
}

void MainWindow::on_checkBox_stateChanged(int arg1)
{
    ui->comboBox_2->clear();
    if (arg1 == 2) {
        ui->pushButton->setEnabled(true);
        QStringList doctors = db->getAllDoctors();
        ui->comboBox_2->addItems(doctors);
        ui->lineEdit_2->setText(ui->comboBox_2->currentText());
        ui->comboBox_2->setCurrentIndex(0);
    } else {
        ui->pushButton->setEnabled(false);
    }
}

void MainWindow::on_comboBox_2_currentIndexChanged(const QString &arg1)
{
    ui->lineEdit_2->setText(arg1);
    QSqlQuery query("SELECT spec FROM doctors WHERE name = '"+arg1+"'");
    query.first();
    QString spec;
    for (int i = 0; i < ui->comboBox_3->count();i++){
        spec = query.value(query.record().indexOf("spec")).toString();
        if(QString::compare(spec, ui->comboBox_3->currentText(),Qt::CaseInsensitive)== 0) {
            ui->comboBox_3->setItemText(i,query.value(query.record().indexOf("spec")).toString());
            ui->comboBox_3->setCurrentIndex(i);
            break;
        }
    }
    ui->label_6->setText(arg1);
    ui->label_7->setText(spec);
    ui->comboBox_3->setCurrentText(spec);
    setCalendar();
}

void MainWindow::on_lineEdit_textEdited(const QString &arg1)
{
    ui->listWidget->clear();
    if(QString::compare(arg1, "",Qt::CaseInsensitive) != 0) {
    QString criterio = ui->comboBox->currentText();
    if (QString::compare(criterio, "Doctor",Qt::CaseInsensitive)== 0) {
        QSqlQuery query("SELECT * FROM desease");// WHERE doctor = ('" + arg1 +"')");
        int idId = query.record().indexOf("ID");
        int doctor = query.record().indexOf("doctor");
        while(query.next()) {
            if (query.value(doctor).toString().contains(arg1))
                selectByCriterio("ID",query.value(idId).toString());

        }
    } else if (QString::compare(criterio, "Type of desease",Qt::CaseInsensitive)== 0) {
        QSqlQuery query("SELECT * FROM desease");// WHERE type = ('" + arg1 +"')");
        int idId = query.record().indexOf("ID");
        int type = query.record().indexOf("type");
        if (query.exec())
        while(query.next()) {
            if (query.value(type).toString().contains(arg1))
                selectByCriterio("ID",query.value(idId).toString());
        }
    } else {
        selectByCriterio(criterio,arg1);
    }
    }
}

void MainWindow::selectByCriterio(QString criterio, QString arg1) {
    QSqlQuery query("SELECT * FROM patients");// WHERE " + criterio + " = ('" + arg1 +"')");
    int index = 0;
    if (QString::compare(criterio, "Surname",Qt::CaseInsensitive)== 0) index = 1;

    if (QString::compare(criterio, "Name",Qt::CaseInsensitive)== 0) index = 2;

    if (QString::compare(criterio, "Status",Qt::CaseInsensitive)== 0) index = 4;
    int row = ui->listWidget->count();
    if (query.exec()) {
        while(query.next()) {
            if(query.value(index).toString().contains(arg1)) {
                QString id = query.value(0).toString();
                QString name = query.value(2).toString();
                QString surname = query.value(1).toString();
                QString itemText = name + " " + surname;
                QVariant data(id);
                QListWidgetItem *newItem = new QListWidgetItem;
                newItem->setData(Qt::UserRole,data);
                newItem->setText(itemText);
                ui->listWidget->insertItem(row,newItem);
            }
        }
    }
    else qDebug() << "error" << query.lastError();
}

void MainWindow::on_listWidget_itemDoubleClicked(QListWidgetItem *item)
{
    QVariant _id = item->data(Qt::UserRole);
    QString id = _id.toString();
    ShowDialog dialog;
    dialog.setData(id);
    dialog.exec();
}

void MainWindow::on_pushButton_6_clicked()
{
    for(int i = 0; i < ui->tableWidget->rowCount();i++) {
        if(ui->tableWidget->item(i,3)->text().toInt() > 17) {
            ui->tableWidget->removeRow(i);
            i--;
        }
    }
}

void MainWindow::on_pushButton_2_clicked()
{
    AboutDialog a;
    a.show();
    a.exec();
}

void MainWindow::on_tabWidget_currentChanged(int index)
{
    if (index == 3) {
        setCalendar();
    }
}

void MainWindow::setCalendar(void) {
    ui->tableWidget_2->setRowCount(0);
    QString doctor = ui->label_6->text();
    QSqlQuery query("SELECT * FROM desease WHERE doctor = '"+doctor+"'");
    int type = query.record().indexOf("type");
    int appointment = query.record().indexOf("appointment");
    int id = query.record().indexOf("ID");
    int row = 0;
    while(query.next()) {
        QString _type = query.value(type).toString();
        QString _appointment= query.value(appointment).toString();
        QString _id= query.value(id).toString();
        QSqlQuery query2("SELECT Surname FROM patients WHERE ID = '"+_id+"'");
        query2.first();
        int surname = query2.record().indexOf("Surname");
        QString _surname= query2.value(surname).toString();
        ui->tableWidget_2->setRowCount(row+1);
        setCalendarRow(_type,_appointment,_id,_surname,row);
        row++;
    }

}

void MainWindow::setCalendarRow(QString _type, QString _appointment, QString _id, QString _surname, int row) {
    auto table = ui->tableWidget_2;
    table->setItem(row,0,new QTableWidgetItem(_id));
    table->setItem(row,1,new QTableWidgetItem(_surname));
    table->setItem(row,2,new QTableWidgetItem(_type));
    table->setItem(row,3,new QTableWidgetItem(_appointment));
}

void MainWindow::on_tableWidget_2_cellDoubleClicked(int row, int column)
{
    QString id = ui->tableWidget->item(row,0)->text();
    ShowDialog dialog;
    dialog.setData(id);
    dialog.exec();
}
