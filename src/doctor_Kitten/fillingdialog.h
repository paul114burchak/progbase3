#ifndef FILLINGDIALOG_H
#define FILLINGDIALOG_H

#include <QDialog>

namespace Ui {
class FillingDialog;
}

class FillingDialog : public QDialog
{
    Q_OBJECT

public:
    explicit FillingDialog(QWidget *parent = 0);
    ~FillingDialog();

    Ui::FillingDialog *ui;
private slots:
    void on_pushButton_clicked();

    void on_checkBox_stateChanged(int arg1);

    void on_comboBox_currentIndexChanged(const QString &arg1);

    void on_checkBox_2_stateChanged(int arg1);

private:
};

#endif // FILLINGDIALOG_H
