#include "showdialog.h"
#include "ui_showdialog.h"
#include <QFileDialog>
#include <QPixmap>
#include "confirmdialog.h"
#include "fillingdialog.h"
#include <QLabel>
#include "database.h"
#include <QDate>
#include <QDebug>
#include <QByteArray>
#include <QListWidget>
#include <QListWidgetItem>
#include <QFile>
#include <QDateTime>
#include <QVariant>
#include <QStringList>
#include <QPdfWriter>
#include <QPainter>
#include <QPrinter>
#include <QTextDocument>

ShowDialog::ShowDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ShowDialog)
{
    ui->setupUi(this);

    ui->dateTimeEdit->setEnabled(false);
    ui->dateEdit->setEnabled(false);
    ui->dateTimeEdit_2->setEnabled(false);
    ui->lineEdit->setEnabled(false);
    ui->lineEdit_2->setEnabled(false);
    ui->lineEdit_3->setEnabled(false);
    ui->lineEdit_4->setEnabled(false);
    ui->pushButton->setEnabled(true);
    ui->pushButton_4->setEnabled(true);
    ui->pushButton_5->setEnabled(true);
    ui->pushButton_6->setEnabled(true);
    ui->label_13->setText(""); 
    ui->pushButton_7->setEnabled(false);
    ui->pushButton_8->setEnabled(false);
    ui->pushButton_2->setText("Edit data");
}

ShowDialog::~ShowDialog()
{
    delete ui;
}

void ShowDialog::on_pushButton_5_clicked()
{
    QString filename = QFileDialog::getOpenFileName(
                this, QString::fromUtf8("Choose a picture to paste")
                ,"/home/firefly",
                "Images (*.jpg *.jpeg *.png *.ico);;");
    QPixmap pix(filename);
    QFile file(filename);
           //QDir::currentPath()

    if (!file.open(QIODevice::ReadOnly)) return;
    ui->picture->setPixmap(pix);
    QString id = this->ui->label_11->text();
    QByteArray inByteArray = file.readAll();

    QSqlQuery query;
    query.prepare("UPDATE images SET picture = (:inByteArray) WHERE ID = (:ID)");
    query.bindValue(":ID",id);
    query.bindValue(":inByteArray",inByteArray);
    if (!query.exec()) {
        qDebug() << "update image error" << query.lastError();
    }

    QSqlQuery query2;
    query2.prepare("UPDATE images SET filename = (:filemane) WHERE ID = (:ID)");
    query2.bindValue(":ID",id);
    query2.bindValue(":filemane",filename);
    if (!query2.exec()) {
        qDebug() << "update filename error" << query.lastError();
    }
}

void ShowDialog::on_pushButton_6_clicked()
{
    QString id = ui->label_11->text();
    database db;
    if (db.deletePatient(id) && db.deleteImage(id) && db.deleteDesease(id)) {
        this->hide();
    } else {
        qDebug() << "error";
    }
    //d->hide();
}

void ShowDialog::on_pushButton_3_clicked()
{
    this->hide();
}

void ShowDialog::on_pushButton_2_clicked()
{
    if (QString::compare(ui->label_13->text(), "Edit mode",Qt::CaseInsensitive)== 0) {
        ui->dateTimeEdit_2->setEnabled(false);
        ui->lineEdit_3->setEnabled(false);
        ui->lineEdit_4->setEnabled(false);
        ui->pushButton->setEnabled(true);
        ui->pushButton_4->setEnabled(true);
        ui->pushButton_5->setEnabled(true);
        ui->pushButton_6->setEnabled(true);
        ui->label_13->setText("");
        ui->pushButton_2->setText("Edit data");
    } else {
        ui->dateTimeEdit_2->setEnabled(true);
        ui->lineEdit_3->setEnabled(true);
        ui->lineEdit_4->setEnabled(true);
        ui->pushButton->setEnabled(false);
        ui->pushButton_4->setEnabled(false);
        ui->pushButton_5->setEnabled(false);
        ui->pushButton_6->setEnabled(false);
        ui->label_13->setText("Edit mode");
        ui->pushButton_2->setText("Edit mode");
    }
}

void ShowDialog::setData(QString id) {
    QSqlQuery query("SELECT * FROM patients");

    int idId = query.record().indexOf("ID");
    int surname = query.record().indexOf("Surname");
    int name = query.record().indexOf("Name");
    int date = query.record().indexOf("Date_birth");
    int status = query.record().indexOf("Status");
    int datetime = query.record().indexOf("Registration_date");
    int sex = query.record().indexOf("sex");
    do {
        if (QString::compare(id, query.value(idId).toString(),Qt::CaseInsensitive)== 0) {
            QString _name = query.value(name).toString();
            QString _surname = query.value(surname).toString();
            QString _status = query.value(status).toString();
            QDate _date = QDate::fromString(query.value(date).toString(),"dd MM yyyy");
            QDateTime _datetime = QDateTime::fromString(query.value(datetime).toString(),"dd.MM.yyyy hh:mm");
            QString _sex = query.value(sex).toString();

            this->ui->label->setText(_surname);
            this->ui->label_2->setText(_name);
            this->ui->lineEdit_4->setText(_status);
            this->ui->dateEdit->setDate(_date);
            this->ui->dateTimeEdit->setDateTime(_datetime);
            this->ui->label_11->setText(id);
            QString title = _name+_surname;
            this->setWindowTitle(title);

            if (QString::compare(_sex, "Male",Qt::CaseInsensitive)== 0) {
                QPixmap pix("/home/firefly/op/progbase3/src/male.png");
                ui->label_10->setPixmap(pix);
            } else {
                QPixmap pix("/home/firefly/op/progbase3/src/female.png");
                ui->label_10->setPixmap(pix);
            }
            break;
        }
   } while(query.next());

   QSqlQuery image;
   image.prepare("SELECT picture FROM images WHERE ID = (:ID)");
   image.bindValue(":ID", id);
   if( !image.exec())
           qDebug() << "Error getting image from table:\n" << image.lastError();
   image.first();
   int byte = image.record().indexOf("picture");
   QByteArray outByteArray = image.value(byte).toByteArray();
   QPixmap outPixmap = QPixmap();
   outPixmap.loadFromData( outByteArray );
   this->ui->picture->setPixmap(outPixmap);

   selectDesease(id);

}

void ShowDialog::selectDesease(QString id) {
    QSqlQuery des("SELECT * FROM desease");
    int Id = des.record().indexOf("ID");
    int type = des.record().indexOf("type");
    int doctor = des.record().indexOf("doctor");
    int __status = des.record().indexOf("status");
    int __date = des.record().indexOf("appointment");
    int row = 0;
    do {
        if (QString::compare(id, des.value(Id).toString(),Qt::CaseInsensitive) == 0) {
             QString _type = des.value(type).toString();
             QString _doctor = des.value(doctor).toString();
             QString _date = des.value(__date).toString();
             QString _status = des.value(__status).toString();
             insertItemToList(_type, _doctor, _date, _status,row);
        }
    } while(des.next());
}

void ShowDialog::on_lineEdit_4_textEdited(const QString &arg1)
{
    QString id = this->ui->label_11->text();
    QSqlQuery query;
    query.prepare("UPDATE patients SET Status = (:Status) WHERE ID = (:ID)");
    query.bindValue(":ID", id);
    query.bindValue(":Status", arg1);
    if (!query.exec()) qDebug() << "status" << query.lastError();
}

void ShowDialog::on_dateTimeEdit_2_dateTimeChanged(const QDateTime &dateTime)
{
    QString appointment = dateTime.toString("dd.MM.yyyy hh:mm");
    QString id = this->ui->label_11->text();
    QString type = ui->lineEdit->text();
    QSqlQuery query;
    query.prepare("UPDATE desease SET appointment = (:appointment) WHERE ID = (:ID) AND type = (:type)");
    query.bindValue(":ID", id);
    query.bindValue(":type",type);
    query.bindValue(":appointment", appointment);
    if (!query.exec()) qDebug() << "datetime" << query.lastError();
}

void ShowDialog::on_lineEdit_3_textEdited(const QString &arg1)
{
    QString id = this->ui->label_11->text();
    QSqlQuery query;
    QString type = ui->lineEdit->text();
    query.prepare("UPDATE desease SET status = (:status) WHERE ID = (:ID) AND type = (:type)");
    query.bindValue(":type",type);
    query.bindValue(":ID", id);
    query.bindValue(":status", arg1);
    if (!query.exec()) qDebug() << "status2" << query.lastError();
}

void ShowDialog::on_pushButton_clicked()
{
    ui->dateTimeEdit_2->setEnabled(true);
    ui->lineEdit_3->setEnabled(true);
    ui->dateTimeEdit_2->setDateTime(QDateTime::currentDateTime());
    ui->lineEdit_3->clear();
    ui->lineEdit->setEnabled(true);
    ui->lineEdit->clear();
    ui->pushButton_7->setEnabled(true);
    ui->pushButton_7->setFlat(false);
    ui->pushButton_7->setText("Confirm");
    ui->pushButton_8->setEnabled(true);
    ui->pushButton_8->setFlat(false);
    ui->pushButton_8->setText("Cancel");
    ui->pushButton_7->setBackgroundRole(QPalette::Highlight);
    ui->label_15->setText("Choose doctor");
    database db;
    QStringList doctors = db.getAllDoctors();
    ui->comboBox->addItems(doctors);
}

void ShowDialog::on_pushButton_4_clicked()
{
    QSqlQuery query;
    query.prepare("SELECT * FROM images WHERE ID = (:ID)");
    query.bindValue(":ID",ui->label_11->text().toInt());
    query.exec();
    query.first();
    int _pic = query.record().indexOf("filename");
    QString pic = query.value(_pic).toString();

    QSqlQuery des("SELECT * FROM desease");
    int Id = des.record().indexOf("ID");
    int type = des.record().indexOf("type");
    int doctor = des.record().indexOf("doctor");
    int __date = des.record().indexOf("appointment");
    QString _type;
    QString _doctor;
    QString _date ;
    do {
    if (QString::compare(ui->label_11->text(), des.value(Id).toString(),Qt::CaseInsensitive) == 0) {
         _type = des.value(type).toString();
         _doctor = des.value(doctor).toString();
         _date = des.value(__date).toString();
         break;
    }
    }while(des.next());

    QString filename = "/home/firefly/op/progbase3/src/" + ui->label->text() + "_" +ui->label_2->text() + ".pdf";
    QString html = "<div align=right>"
                   "Kyiv, " + QDate::currentDate().toString("dd.MM.yyyy") + " "
                "</div>"
                "<div align=center>"
                   "<h1 align=center>Dr. Kitten</h1>"
                "</div>"
                "<hr>"
                "<div style=\"width:400px;height:400px;float:left\">"
                   "Patient #" + ui->label_11->text() +"<br>"
                   "Name - " + ui->label_2->text() +"<br>"
                   "Surname - " + ui->label->text() +"<br>"
                   "Status - " + ui->lineEdit_4->text() +"<br>"
                   "Date of Birth - " + ui->dateTimeEdit->dateTime().toString("dd.MM.yyyy hh:mm") +"<br>"
                   "Last desease:<br>"
                   "<pre>"
                       "    Type - " + _type +"<br>"
                       "    Doctor - "+ _doctor +"<br>"
                       "    Appointment date - "+ _date +"<br>"
                   "</pre>"
                "</div>"

                "<div align=right\">"
                   "<img src="+pic+" height=\"300\" height=\"200\">"
                "</div>";
    QTextDocument document;
    document.setHtml(html);

    QPrinter printer(QPrinter::PrinterResolution);
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setPaperSize(QPrinter::A4);
    printer.setOutputFileName(filename);
    printer.setPageMargins(QMarginsF(15, 15, 15, 15));

    document.print(&printer);

}

void ShowDialog::on_listWidget_itemSelectionChanged()
{
    QListWidgetItem* item = ui->listWidget->currentItem();
    QVariant data = item->data(Qt::UserRole);
    QStringList outData = data.toStringList();
    ui->lineEdit->setText(outData.at(0));
    ui->lineEdit_2->setText(outData.at(1));
    ui->lineEdit_3->setText(outData.at(3));
    QDateTime datetime = QDateTime::fromString(outData.at(2),"dd.MM.yyyy hh:mm");
    ui->dateTimeEdit_2->setDateTime(datetime);
}

void ShowDialog::on_pushButton_7_clicked()
{
    int id = this->ui->label_11->text().toInt();
    QString type = ui->lineEdit->text();
    QString doctor = ui->lineEdit_2->text();
    QString status = ui->lineEdit_3->text();
    QString appointment = ui->dateTimeEdit_2->dateTime().toString("dd.MM.yyyy hh:mm");
    database db;
    db.insertDesease(id,type,doctor,appointment,status);
    int row = ui->listWidget->count();
    insertItemToList(type, doctor, appointment, status,row);
    clear();
}

void ShowDialog::insertItemToList(QString _type, QString _doctor, QString _appointment, QString _status, int row) {
    QStringList item = (QStringList() << _type << _doctor << _appointment << _status);
    QVariant data(item);
    QListWidgetItem *newItem = new QListWidgetItem;
    newItem->setData(Qt::UserRole,data);
    newItem->setText(_type);
    ui->listWidget->insertItem(row,newItem);
    row++;
}

void ShowDialog::on_comboBox_currentTextChanged(const QString &arg1)
{
    ui->lineEdit_2->setText(arg1);
}

void ShowDialog::on_pushButton_8_clicked()
{
    clear();
}

void ShowDialog::clear(){
    ui->pushButton_7->setEnabled(false);
    ui->pushButton_7->setFlat(true);
    ui->pushButton_7->setText("");
    ui->pushButton_8->setEnabled(false);
    ui->pushButton_8->setFlat(true);
    ui->pushButton_8->setText("");
    ui->pushButton_7->setBackgroundRole(QPalette::Window);
    ui->label_15->setText("");
    ui->dateTimeEdit_2->setEnabled(false);
    ui->lineEdit_3->setEnabled(false);
    ui->dateTimeEdit_2->setDateTime(QDateTime::currentDateTime());
    ui->lineEdit_3->clear();
    ui->lineEdit->clear();
    ui->lineEdit->setEnabled(false);
    ui->comboBox->clear();
}
