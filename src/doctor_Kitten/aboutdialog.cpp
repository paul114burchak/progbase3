#include "aboutdialog.h"
#include "ui_aboutdialog.h"

AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog)
{
    ui->setupUi(this);
    ui->label_4->setText("<a href=\"https://docs.google.com/document/d/1lJjkICk1BpxNuo-hcbn9cHDWGe1JSjIQCpWdtRu_DLw/edit\">Technical specifications</a>");
    ui->label_4->setTextFormat(Qt::RichText);
    ui->label_4->setTextInteractionFlags(Qt::TextBrowserInteraction);
    ui->label_4->setOpenExternalLinks(true);

    ui->label_5->setText("<a href=\"https://docs.google.com/document/d/1rR7vWNiRW0wdL5Fj7jRQEHbJNZyDU7oMiBN6If9WWJI/edit\">Report</a>");
    ui->label_5->setTextFormat(Qt::RichText);
    ui->label_5->setTextInteractionFlags(Qt::TextBrowserInteraction);
    ui->label_5->setOpenExternalLinks(true);

}

AboutDialog::~AboutDialog()
{
    delete ui;
}
