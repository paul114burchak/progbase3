#-------------------------------------------------
#
# Project created by QtCreator 2017-04-19T19:40:55
#
#-------------------------------------------------

QT += core gui
QT += sql
QT += gui
QT += printsupport
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = doctor_Kitten
TEMPLATE = app


CONFIG +=c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    fillingdialog.cpp \
    database.cpp \
    showdialog.cpp \
    confirmdialog.cpp \
    aboutdialog.cpp

HEADERS  += mainwindow.h \
    fillingdialog.h \
    database.h \
    showdialog.h \
    confirmdialog.h \
    aboutdialog.h

FORMS    += mainwindow.ui \
    fillingdialog.ui \
    showdialog.ui \
    confirmdialog.ui \
    aboutdialog.ui

RESOURCES += \
    res.qrc
