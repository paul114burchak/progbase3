/********************************************************************************
** Form generated from reading UI file 'showdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SHOWDIALOG_H
#define UI_SHOWDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDateTimeEdit>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ShowDialog
{
public:
    QLabel *label_3;
    QListWidget *listWidget;
    QLabel *picture;
    QLabel *label_13;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *pushButton_5;
    QPushButton *pushButton_4;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_6;
    QPushButton *pushButton_3;
    QWidget *layoutWidget1;
    QGridLayout *gridLayout;
    QLabel *label_8;
    QLineEdit *lineEdit_2;
    QLabel *label_9;
    QLineEdit *lineEdit_3;
    QLabel *label_12;
    QLabel *label_14;
    QLineEdit *lineEdit;
    QDateTimeEdit *dateTimeEdit_2;
    QWidget *layoutWidget2;
    QFormLayout *formLayout;
    QLabel *label_4;
    QDateEdit *dateEdit;
    QLabel *label_5;
    QDateTimeEdit *dateTimeEdit;
    QLabel *label_6;
    QLineEdit *lineEdit_4;
    QWidget *layoutWidget3;
    QGridLayout *gridLayout_2;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_7;
    QLabel *label_11;
    QLabel *label_10;
    QPushButton *pushButton_7;
    QComboBox *comboBox;
    QLabel *label_15;
    QPushButton *pushButton_8;

    void setupUi(QDialog *ShowDialog)
    {
        if (ShowDialog->objectName().isEmpty())
            ShowDialog->setObjectName(QStringLiteral("ShowDialog"));
        ShowDialog->resize(766, 430);
        label_3 = new QLabel(ShowDialog);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(180, 190, 67, 17));
        listWidget = new QListWidget(ShowDialog);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setGeometry(QRect(160, 210, 256, 192));
        picture = new QLabel(ShowDialog);
        picture->setObjectName(QStringLiteral("picture"));
        picture->setGeometry(QRect(10, 10, 171, 171));
        picture->setFrameShape(QFrame::WinPanel);
        picture->setLineWidth(8);
        picture->setAlignment(Qt::AlignCenter);
        label_13 = new QLabel(ShowDialog);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(670, 10, 81, 17));
        QPalette palette;
        QBrush brush(QColor(255, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Active, QPalette::Light, brush);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush);
        palette.setBrush(QPalette::Active, QPalette::Dark, brush);
        palette.setBrush(QPalette::Active, QPalette::Mid, brush);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        QBrush brush1(QColor(231, 231, 231, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Light, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Dark, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Mid, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Light, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Dark, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Mid, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush);
        label_13->setPalette(palette);
        QFont font;
        font.setFamily(QStringLiteral("URW Gothic L"));
        font.setPointSize(12);
        font.setBold(true);
        font.setItalic(true);
        font.setWeight(75);
        label_13->setFont(font);
        layoutWidget = new QWidget(ShowDialog);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 207, 134, 191));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        pushButton_5 = new QPushButton(layoutWidget);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));

        verticalLayout->addWidget(pushButton_5);

        pushButton_4 = new QPushButton(layoutWidget);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));

        verticalLayout->addWidget(pushButton_4);

        pushButton = new QPushButton(layoutWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        verticalLayout->addWidget(pushButton);

        pushButton_2 = new QPushButton(layoutWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        verticalLayout->addWidget(pushButton_2);

        pushButton_6 = new QPushButton(layoutWidget);
        pushButton_6->setObjectName(QStringLiteral("pushButton_6"));

        verticalLayout->addWidget(pushButton_6);

        pushButton_3 = new QPushButton(layoutWidget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));

        verticalLayout->addWidget(pushButton_3);

        layoutWidget1 = new QWidget(ShowDialog);
        layoutWidget1->setObjectName(QStringLiteral("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(440, 210, 311, 185));
        gridLayout = new QGridLayout(layoutWidget1);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        label_8 = new QLabel(layoutWidget1);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout->addWidget(label_8, 1, 0, 1, 1);

        lineEdit_2 = new QLineEdit(layoutWidget1);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));

        gridLayout->addWidget(lineEdit_2, 1, 1, 1, 2);

        label_9 = new QLabel(layoutWidget1);
        label_9->setObjectName(QStringLiteral("label_9"));

        gridLayout->addWidget(label_9, 2, 0, 1, 1);

        lineEdit_3 = new QLineEdit(layoutWidget1);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));

        gridLayout->addWidget(lineEdit_3, 3, 1, 1, 2);

        label_12 = new QLabel(layoutWidget1);
        label_12->setObjectName(QStringLiteral("label_12"));

        gridLayout->addWidget(label_12, 3, 0, 1, 1);

        label_14 = new QLabel(layoutWidget1);
        label_14->setObjectName(QStringLiteral("label_14"));

        gridLayout->addWidget(label_14, 0, 0, 1, 1);

        lineEdit = new QLineEdit(layoutWidget1);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        gridLayout->addWidget(lineEdit, 0, 1, 1, 2);

        dateTimeEdit_2 = new QDateTimeEdit(layoutWidget1);
        dateTimeEdit_2->setObjectName(QStringLiteral("dateTimeEdit_2"));

        gridLayout->addWidget(dateTimeEdit_2, 2, 1, 1, 2);

        layoutWidget2 = new QWidget(ShowDialog);
        layoutWidget2->setObjectName(QStringLiteral("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(251, 90, 311, 91));
        formLayout = new QFormLayout(layoutWidget2);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        label_4 = new QLabel(layoutWidget2);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label_4);

        dateEdit = new QDateEdit(layoutWidget2);
        dateEdit->setObjectName(QStringLiteral("dateEdit"));

        formLayout->setWidget(0, QFormLayout::FieldRole, dateEdit);

        label_5 = new QLabel(layoutWidget2);
        label_5->setObjectName(QStringLiteral("label_5"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_5);

        dateTimeEdit = new QDateTimeEdit(layoutWidget2);
        dateTimeEdit->setObjectName(QStringLiteral("dateTimeEdit"));

        formLayout->setWidget(1, QFormLayout::FieldRole, dateTimeEdit);

        label_6 = new QLabel(layoutWidget2);
        label_6->setObjectName(QStringLiteral("label_6"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_6);

        lineEdit_4 = new QLineEdit(layoutWidget2);
        lineEdit_4->setObjectName(QStringLiteral("lineEdit_4"));

        formLayout->setWidget(2, QFormLayout::FieldRole, lineEdit_4);

        layoutWidget3 = new QWidget(ShowDialog);
        layoutWidget3->setObjectName(QStringLiteral("layoutWidget3"));
        layoutWidget3->setGeometry(QRect(256, 20, 211, 61));
        gridLayout_2 = new QGridLayout(layoutWidget3);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(layoutWidget3);
        label->setObjectName(QStringLiteral("label"));
        QFont font1;
        font1.setFamily(QStringLiteral("URW Gothic L"));
        font1.setPointSize(14);
        font1.setBold(true);
        font1.setItalic(true);
        font1.setWeight(75);
        label->setFont(font1);

        gridLayout_2->addWidget(label, 0, 0, 1, 1);

        label_2 = new QLabel(layoutWidget3);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setFont(font1);

        gridLayout_2->addWidget(label_2, 0, 1, 1, 1);

        label_7 = new QLabel(layoutWidget3);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setFont(font);

        gridLayout_2->addWidget(label_7, 1, 0, 1, 1);

        label_11 = new QLabel(layoutWidget3);
        label_11->setObjectName(QStringLiteral("label_11"));
        QFont font2;
        font2.setFamily(QStringLiteral("URW Gothic L"));
        font2.setPointSize(12);
        font2.setBold(true);
        font2.setItalic(true);
        font2.setUnderline(true);
        font2.setWeight(75);
        label_11->setFont(font2);

        gridLayout_2->addWidget(label_11, 1, 1, 1, 1);

        label_10 = new QLabel(ShowDialog);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(570, 10, 91, 151));
        label_10->setAlignment(Qt::AlignCenter);
        pushButton_7 = new QPushButton(ShowDialog);
        pushButton_7->setObjectName(QStringLiteral("pushButton_7"));
        pushButton_7->setGeometry(QRect(670, 170, 89, 25));
        pushButton_7->setFlat(true);
        comboBox = new QComboBox(ShowDialog);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(550, 400, 201, 25));
        comboBox->setMaxVisibleItems(20);
        comboBox->setFrame(true);
        comboBox->setModelColumn(0);
        label_15 = new QLabel(ShowDialog);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setGeometry(QRect(440, 400, 101, 17));
        pushButton_8 = new QPushButton(ShowDialog);
        pushButton_8->setObjectName(QStringLiteral("pushButton_8"));
        pushButton_8->setGeometry(QRect(570, 170, 91, 25));
        pushButton_8->setFlat(true);

        retranslateUi(ShowDialog);

        pushButton_7->setDefault(false);


        QMetaObject::connectSlotsByName(ShowDialog);
    } // setupUi

    void retranslateUi(QDialog *ShowDialog)
    {
        ShowDialog->setWindowTitle(QApplication::translate("ShowDialog", "Dialog", 0));
        label_3->setText(QApplication::translate("ShowDialog", "History:", 0));
        picture->setText(QString());
        label_13->setText(QApplication::translate("ShowDialog", "Edit mode", 0));
        pushButton_5->setText(QApplication::translate("ShowDialog", "Add picture", 0));
        pushButton_4->setText(QApplication::translate("ShowDialog", "Cure out", 0));
        pushButton->setText(QApplication::translate("ShowDialog", "New appointment", 0));
        pushButton_2->setText(QApplication::translate("ShowDialog", "Edit data", 0));
        pushButton_6->setText(QApplication::translate("ShowDialog", "Delete", 0));
        pushButton_3->setText(QApplication::translate("ShowDialog", "Ok", 0));
        label_8->setText(QApplication::translate("ShowDialog", "Doctor", 0));
        label_9->setText(QApplication::translate("ShowDialog", "Start treatment", 0));
        label_12->setText(QApplication::translate("ShowDialog", "Desease status", 0));
        label_14->setText(QApplication::translate("ShowDialog", "Type", 0));
        dateTimeEdit_2->setDisplayFormat(QApplication::translate("ShowDialog", "dd.MM.yyyy HH:mm", 0));
        label_4->setText(QApplication::translate("ShowDialog", "Date of birth", 0));
        dateEdit->setDisplayFormat(QApplication::translate("ShowDialog", "dd.MM.yyyy", 0));
        label_5->setText(QApplication::translate("ShowDialog", "Check in", 0));
        dateTimeEdit->setDisplayFormat(QApplication::translate("ShowDialog", "dd.MM.yyyy HH:mm", 0));
        label_6->setText(QApplication::translate("ShowDialog", "Status", 0));
        label->setText(QApplication::translate("ShowDialog", "Surname", 0));
        label_2->setText(QApplication::translate("ShowDialog", "Name", 0));
        label_7->setText(QApplication::translate("ShowDialog", "ID", 0));
        label_11->setText(QApplication::translate("ShowDialog", "0001", 0));
        label_10->setText(QString());
        pushButton_7->setText(QString());
        label_15->setText(QString());
        pushButton_8->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class ShowDialog: public Ui_ShowDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SHOWDIALOG_H
