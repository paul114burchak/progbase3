/****************************************************************************
** Meta object code from reading C++ file 'showdialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../doctor_Kitten/showdialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'showdialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_ShowDialog_t {
    QByteArrayData data[17];
    char stringdata0[366];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ShowDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ShowDialog_t qt_meta_stringdata_ShowDialog = {
    {
QT_MOC_LITERAL(0, 0, 10), // "ShowDialog"
QT_MOC_LITERAL(1, 11, 23), // "on_pushButton_5_clicked"
QT_MOC_LITERAL(2, 35, 0), // ""
QT_MOC_LITERAL(3, 36, 23), // "on_pushButton_6_clicked"
QT_MOC_LITERAL(4, 60, 23), // "on_pushButton_3_clicked"
QT_MOC_LITERAL(5, 84, 23), // "on_pushButton_2_clicked"
QT_MOC_LITERAL(6, 108, 24), // "on_lineEdit_4_textEdited"
QT_MOC_LITERAL(7, 133, 4), // "arg1"
QT_MOC_LITERAL(8, 138, 33), // "on_dateTimeEdit_2_dateTimeCha..."
QT_MOC_LITERAL(9, 172, 8), // "dateTime"
QT_MOC_LITERAL(10, 181, 24), // "on_lineEdit_3_textEdited"
QT_MOC_LITERAL(11, 206, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(12, 228, 23), // "on_pushButton_4_clicked"
QT_MOC_LITERAL(13, 252, 34), // "on_listWidget_itemSelectionCh..."
QT_MOC_LITERAL(14, 287, 23), // "on_pushButton_7_clicked"
QT_MOC_LITERAL(15, 311, 30), // "on_comboBox_currentTextChanged"
QT_MOC_LITERAL(16, 342, 23) // "on_pushButton_8_clicked"

    },
    "ShowDialog\0on_pushButton_5_clicked\0\0"
    "on_pushButton_6_clicked\0on_pushButton_3_clicked\0"
    "on_pushButton_2_clicked\0"
    "on_lineEdit_4_textEdited\0arg1\0"
    "on_dateTimeEdit_2_dateTimeChanged\0"
    "dateTime\0on_lineEdit_3_textEdited\0"
    "on_pushButton_clicked\0on_pushButton_4_clicked\0"
    "on_listWidget_itemSelectionChanged\0"
    "on_pushButton_7_clicked\0"
    "on_comboBox_currentTextChanged\0"
    "on_pushButton_8_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ShowDialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   79,    2, 0x08 /* Private */,
       3,    0,   80,    2, 0x08 /* Private */,
       4,    0,   81,    2, 0x08 /* Private */,
       5,    0,   82,    2, 0x08 /* Private */,
       6,    1,   83,    2, 0x08 /* Private */,
       8,    1,   86,    2, 0x08 /* Private */,
      10,    1,   89,    2, 0x08 /* Private */,
      11,    0,   92,    2, 0x08 /* Private */,
      12,    0,   93,    2, 0x08 /* Private */,
      13,    0,   94,    2, 0x08 /* Private */,
      14,    0,   95,    2, 0x08 /* Private */,
      15,    1,   96,    2, 0x08 /* Private */,
      16,    0,   99,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    7,
    QMetaType::Void, QMetaType::QDateTime,    9,
    QMetaType::Void, QMetaType::QString,    7,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    7,
    QMetaType::Void,

       0        // eod
};

void ShowDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ShowDialog *_t = static_cast<ShowDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_pushButton_5_clicked(); break;
        case 1: _t->on_pushButton_6_clicked(); break;
        case 2: _t->on_pushButton_3_clicked(); break;
        case 3: _t->on_pushButton_2_clicked(); break;
        case 4: _t->on_lineEdit_4_textEdited((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: _t->on_dateTimeEdit_2_dateTimeChanged((*reinterpret_cast< const QDateTime(*)>(_a[1]))); break;
        case 6: _t->on_lineEdit_3_textEdited((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 7: _t->on_pushButton_clicked(); break;
        case 8: _t->on_pushButton_4_clicked(); break;
        case 9: _t->on_listWidget_itemSelectionChanged(); break;
        case 10: _t->on_pushButton_7_clicked(); break;
        case 11: _t->on_comboBox_currentTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 12: _t->on_pushButton_8_clicked(); break;
        default: ;
        }
    }
}

const QMetaObject ShowDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_ShowDialog.data,
      qt_meta_data_ShowDialog,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *ShowDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ShowDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_ShowDialog.stringdata0))
        return static_cast<void*>(const_cast< ShowDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int ShowDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
