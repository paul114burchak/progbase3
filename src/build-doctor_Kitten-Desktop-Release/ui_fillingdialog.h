/********************************************************************************
** Form generated from reading UI file 'fillingdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FILLINGDIALOG_H
#define UI_FILLINGDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDateTimeEdit>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FillingDialog
{
public:
    QCheckBox *checkBox;
    QWidget *layoutWidget;
    QFormLayout *formLayout;
    QLabel *label;
    QLineEdit *lineEdit_1;
    QLineEdit *lineEdit_2;
    QLabel *label_3;
    QDateEdit *dateEdit;
    QLabel *label_2;
    QLabel *label_10;
    QComboBox *comboBox_3;
    QLabel *label_5;
    QLabel *label_7;
    QComboBox *comboBox;
    QLabel *label_8;
    QComboBox *comboBox_2;
    QCheckBox *checkBox_2;
    QLabel *label_9;
    QDateTimeEdit *dateTimeEdit;
    QWidget *layoutWidget1;
    QHBoxLayout *horizontalLayout;
    QDialogButtonBox *buttonBox;
    QPushButton *pushButton;
    QLabel *label_11;
    QLabel *label_12;
    QLabel *label_13;
    QLineEdit *lineEdit;

    void setupUi(QDialog *FillingDialog)
    {
        if (FillingDialog->objectName().isEmpty())
            FillingDialog->setObjectName(QStringLiteral("FillingDialog"));
        FillingDialog->resize(392, 505);
        FillingDialog->setStyleSheet(QStringLiteral("font: 63 oblique 11pt \"URW Gothic L\";"));
        checkBox = new QCheckBox(FillingDialog);
        checkBox->setObjectName(QStringLiteral("checkBox"));
        checkBox->setGeometry(QRect(40, 400, 321, 22));
        checkBox->setChecked(false);
        checkBox->setAutoRepeat(false);
        layoutWidget = new QWidget(FillingDialog);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(50, 40, 281, 132));
        formLayout = new QFormLayout(layoutWidget);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(layoutWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        lineEdit_1 = new QLineEdit(layoutWidget);
        lineEdit_1->setObjectName(QStringLiteral("lineEdit_1"));

        formLayout->setWidget(0, QFormLayout::FieldRole, lineEdit_1);

        lineEdit_2 = new QLineEdit(layoutWidget);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));

        formLayout->setWidget(1, QFormLayout::FieldRole, lineEdit_2);

        label_3 = new QLabel(layoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        formLayout->setWidget(2, QFormLayout::LabelRole, label_3);

        dateEdit = new QDateEdit(layoutWidget);
        dateEdit->setObjectName(QStringLiteral("dateEdit"));
        dateEdit->setProperty("showGroupSeparator", QVariant(false));

        formLayout->setWidget(2, QFormLayout::FieldRole, dateEdit);

        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        label_10 = new QLabel(layoutWidget);
        label_10->setObjectName(QStringLiteral("label_10"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_10);

        comboBox_3 = new QComboBox(layoutWidget);
        comboBox_3->setObjectName(QStringLiteral("comboBox_3"));

        formLayout->setWidget(3, QFormLayout::FieldRole, comboBox_3);

        label_5 = new QLabel(FillingDialog);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(340, 110, 16, 17));
        QPalette palette;
        QBrush brush(QColor(255, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Active, QPalette::Light, brush);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush);
        palette.setBrush(QPalette::Active, QPalette::Dark, brush);
        palette.setBrush(QPalette::Active, QPalette::Mid, brush);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        QBrush brush1(QColor(231, 231, 231, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Light, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Dark, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Mid, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Light, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Dark, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Mid, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush);
        label_5->setPalette(palette);
        label_5->setStyleSheet(QStringLiteral("font: 57 italic 14pt \"Ubuntu\";"));
        label_7 = new QLabel(FillingDialog);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(50, 190, 101, 17));
        comboBox = new QComboBox(FillingDialog);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(40, 210, 231, 25));
        label_8 = new QLabel(FillingDialog);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(50, 250, 81, 17));
        comboBox_2 = new QComboBox(FillingDialog);
        comboBox_2->setObjectName(QStringLiteral("comboBox_2"));
        comboBox_2->setGeometry(QRect(40, 270, 231, 25));
        checkBox_2 = new QCheckBox(FillingDialog);
        checkBox_2->setObjectName(QStringLiteral("checkBox_2"));
        checkBox_2->setGeometry(QRect(40, 370, 92, 23));
        label_9 = new QLabel(FillingDialog);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(30, 340, 181, 17));
        dateTimeEdit = new QDateTimeEdit(FillingDialog);
        dateTimeEdit->setObjectName(QStringLiteral("dateTimeEdit"));
        dateTimeEdit->setGeometry(QRect(220, 340, 141, 26));
        layoutWidget1 = new QWidget(FillingDialog);
        layoutWidget1->setObjectName(QStringLiteral("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(40, 440, 331, 30));
        horizontalLayout = new QHBoxLayout(layoutWidget1);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        buttonBox = new QDialogButtonBox(layoutWidget1);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        horizontalLayout->addWidget(buttonBox);

        pushButton = new QPushButton(layoutWidget1);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        horizontalLayout->addWidget(pushButton);

        label_11 = new QLabel(FillingDialog);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(150, 10, 111, 17));
        label_12 = new QLabel(FillingDialog);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(340, 80, 16, 17));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Active, QPalette::Light, brush);
        palette1.setBrush(QPalette::Active, QPalette::Midlight, brush);
        palette1.setBrush(QPalette::Active, QPalette::Dark, brush);
        palette1.setBrush(QPalette::Active, QPalette::Mid, brush);
        palette1.setBrush(QPalette::Active, QPalette::Text, brush);
        palette1.setBrush(QPalette::Active, QPalette::ButtonText, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Light, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Midlight, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Dark, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Mid, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::ButtonText, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::Light, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::Midlight, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::Dark, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::Mid, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::Text, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::ButtonText, brush);
        label_12->setPalette(palette1);
        label_12->setStyleSheet(QStringLiteral("font: 57 italic 14pt \"Ubuntu\";"));
        label_13 = new QLabel(FillingDialog);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(340, 50, 16, 17));
        QPalette palette2;
        palette2.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette2.setBrush(QPalette::Active, QPalette::Light, brush);
        palette2.setBrush(QPalette::Active, QPalette::Midlight, brush);
        palette2.setBrush(QPalette::Active, QPalette::Dark, brush);
        palette2.setBrush(QPalette::Active, QPalette::Mid, brush);
        palette2.setBrush(QPalette::Active, QPalette::Text, brush);
        palette2.setBrush(QPalette::Active, QPalette::ButtonText, brush1);
        palette2.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::Light, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::Midlight, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::Dark, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::Mid, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::ButtonText, brush1);
        palette2.setBrush(QPalette::Disabled, QPalette::WindowText, brush);
        palette2.setBrush(QPalette::Disabled, QPalette::Light, brush);
        palette2.setBrush(QPalette::Disabled, QPalette::Midlight, brush);
        palette2.setBrush(QPalette::Disabled, QPalette::Dark, brush);
        palette2.setBrush(QPalette::Disabled, QPalette::Mid, brush);
        palette2.setBrush(QPalette::Disabled, QPalette::Text, brush);
        palette2.setBrush(QPalette::Disabled, QPalette::ButtonText, brush);
        label_13->setPalette(palette2);
        label_13->setStyleSheet(QStringLiteral("font: 57 italic 14pt \"Ubuntu\";"));
        lineEdit = new QLineEdit(FillingDialog);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(50, 300, 211, 25));

        retranslateUi(FillingDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), FillingDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), FillingDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(FillingDialog);
    } // setupUi

    void retranslateUi(QDialog *FillingDialog)
    {
        FillingDialog->setWindowTitle(QApplication::translate("FillingDialog", "Add patient", 0));
        checkBox->setText(QApplication::translate("FillingDialog", "Has no complaints, check in only", 0));
        label->setText(QApplication::translate("FillingDialog", "Name:", 0));
        label_3->setText(QApplication::translate("FillingDialog", "Date of birth:", 0));
        dateEdit->setDisplayFormat(QApplication::translate("FillingDialog", "dd.MM.yyyy", 0));
        label_2->setText(QApplication::translate("FillingDialog", "Surname:", 0));
        label_10->setText(QApplication::translate("FillingDialog", "Sex:", 0));
        comboBox_3->clear();
        comboBox_3->insertItems(0, QStringList()
         << QApplication::translate("FillingDialog", "Male", 0)
         << QApplication::translate("FillingDialog", "Female", 0)
        );
        label_5->setText(QApplication::translate("FillingDialog", "*", 0));
        label_7->setText(QApplication::translate("FillingDialog", "Problem with:", 0));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("FillingDialog", "Head", 0)
         << QApplication::translate("FillingDialog", "Teeth", 0)
         << QApplication::translate("FillingDialog", "Heart", 0)
         << QApplication::translate("FillingDialog", "Breathing", 0)
         << QApplication::translate("FillingDialog", "Skin", 0)
         << QApplication::translate("FillingDialog", "Body temperature", 0)
         << QApplication::translate("FillingDialog", "Another", 0)
        );
        label_8->setText(QApplication::translate("FillingDialog", "Symptoms:", 0));
        checkBox_2->setText(QApplication::translate("FillingDialog", "First aid", 0));
        label_9->setText(QApplication::translate("FillingDialog", "Appointment date/time", 0));
        dateTimeEdit->setDisplayFormat(QApplication::translate("FillingDialog", "dd.MM.yyyy HH:mm", 0));
        pushButton->setText(QApplication::translate("FillingDialog", "Clear", 0));
        label_11->setText(QApplication::translate("FillingDialog", "ADD PATIENT", 0));
        label_12->setText(QApplication::translate("FillingDialog", "*", 0));
        label_13->setText(QApplication::translate("FillingDialog", "*", 0));
    } // retranslateUi

};

namespace Ui {
    class FillingDialog: public Ui_FillingDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FILLINGDIALOG_H
